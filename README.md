University of Dayton

Department of Computer Science

CPS 491 - Spring 2022

Team 2


# Capstone II Project - Balloon Federation of America: Wind Explorer

# Team members

1.  Asia Solomianko, <solomiankoj1@udayton.edu>
2.  Mick Ward, <wardc11@udayton.edu>
3.  Katie Weidner, <weidnerk1@udayton.edu>
4.  Nathan Stull, <stulln2@udayton.edu>


# Company Mentors

Al Nels, Vice Chair; 

Randy Wells;

Fred Dinkler;

Balloon Federation of America

1601 N.Jefferson Way, Indianola, Iowa 50125

## Project homepage

https://cps491s22-team2.bitbucket.io


# Overview

User will have to be an authenticated BFA member to use the app

Users then can use the app to predict wind patterns, projected balloon landings, projected balloon path

Users can then upload the current data to Google Drive for live updates for other users

Account information and current findings are then saved into the database


![Overview Architecture](https://trello.com/1/cards/61b664785829f94bb74e379d/attachments/61b66495cd3c9c4140fde80c/download/Overview.png "A Sample of Overview Architecture")

Figure 1. - A Sample of Overview Architecture of the proposed project.

# Project Context and Scope

Our project is going to be a mobile app for Android and iOS. By the end of the semester, we plan to have the app available on the Google Play Store as well as the App Store. The motivation for the project was that our group was interested in learning more about the world of mobile app development. An opportunity to make an app around something exciting like balloon piloting sounded perfect, so we decided to go for it.
Our app will be used by hot air balloon pilots all over the world, and will allow for safer and more accurate flight patterns.


# High-level Requirements

Users should be able to login to their BFA accounts to be authenticated

App should have accurate wind projections

App should be able to make calculations on flight patterns for balloons

App should be able to provide a hodograph

App should store calculated values in a database

Calculations should be taken in different, customized measurements


# Technology

We are going to be using ReactNative as the framework to create our app. Using JavaScript, we can use this single framework to create both an iOS and Android application. JavaScript is very helpful for interactive behavior within the app such as zooming, showing/hiding information, animations, and timers. We also have a lot of various libraries we can utilize throughout our project.

Expo CLI is a tool we use to run our project on a server. With the use of the Expo Go app, we can scan the QR code that appears when we run our program. This opens our app in a simulator. This allows us to see the current status of our application without deploying it everytime.

The BFA has provided us with two Github repositories, one for the iOS app and one for the Android app. We plan to incorporate this code into our app, using mostly the calculations found within the app. We will also pull some of their images to use in our app. This will allow for the look and feel of the app to be similar to the ones they used or developed in the past.

## Postman
A platform used for building and using APIs. This is used in order to test and access the API created by Fred Dinkler.

## Building the APK or publishing to IOS Test Flight
Follow this website to help with building an APK or building the ios app needed to publish on test flight <https://docs.expo.dev/classic/building-standalone-apps/>. to make an adroid apk you can use expo build:android -t apk to build an apk file that can be donwloaded from the computer to the phone. to make an app bundle you can use expo build:android -t app-bundle. The app bundle is recommended in order to publish it to the play store.

 

# Impacts

Our app will be able to provide accurate and helpful projections for hot air balloon paths based off of wind projections. This will allow for safer and more accurate flights.

The app's scope may also be extended beyond just balloons, and may be able to track other things, such as toxic gas pollution.

As a team, we will gain experience on developing an app from scratch. With the BFA wanting an updated APK of their Android app, we also gain experience on how to create an APK and troubleshooting other people's code.

# Project Management

* Website bitbucket repository: https://bitbucket.org/cps491s22-team2/cps491s22-team2.bitbucket.io/src/main/
* Code bitbucket repoository: https://bitbucket.org/cps491s22-team2/cps491s22-team2/src/master/
* Trello Board: https://trello.com/b/vRJsAVtu/sprint-board


Here is our Trello board timeline (Gantt chart) with sprint cycles for Spring 2022: 

![Spring 2022 Timeline on Trello](https://trello.com/1/cards/61b664785829f94bb74e379d/attachments/61b666d49fd9642b14a98234/download/SprintPlan.png "Spring 2022 Timeline")


# Implementation

Through React Native, we are using navigation containers to go from page to page. This is how we set up our framework. We have different components of the different screens we should have.

The main part of this cycle was to get the login working. Using the BFA API, we can connect to their server and authenticate the users properly.
![BFA API connection](https://trello.com/1/cards/61b664785829f94bb74e379d/attachments/62017d0e7337357b361a9b83/download/APIrequest.png "API connection")

Below we see the code for checking for a 200 OK response. We will also check for proper subscriptions and data plan number to make sure the user isn't getting access to this account on too many devices.
![Login Authentication](https://trello.com/1/cards/61b664785829f94bb74e379d/attachments/62017d106b57587e81f65865/download/LoginAuth.png "Login Authentication")

Here we see the code for the camera and the pressable buttons.
![Camera Code](https://trello.com/1/cards/61b664785829f94bb74e379d/attachments/621d358662e78b4240ea697c/download/CameraPressButtonCode.png "Camera")

When the timer button is pressed, it starts and stops based on whether the timer is already counting.
![Timer Code](https://trello.com/1/cards/61b664785829f94bb74e379d/attachments/621d35872ba38e2b1d5760fd/download/TimerCode.png "Timer")

With the iOS and Android app reacting differently, we have to do separate UI for the two platforms. For example, the camera for Android cannot be changed to a circle. In this case, we have to add an overlay of a circle cutout for the camera to appear as a circle. Here is an example of checking what platform the app is being run on:

`{Platform.OS === 'android' ? <Image source={require('./camera_circle_cutout.png')} resizeMode={"cover"} style={styles.cameraCutout}></Image>: null }` 


# Demo Screenshots
Login Screen
![Login Screen](https://trello.com/1/cards/61b664785829f94bb74e379d/attachments/621d2f6abda6cc390a192ebb/download/Login.PNG "Login Screen")

First Page Screen
![First Page Screen](https://trello.com/1/cards/61b664785829f94bb74e379d/attachments/621d2f6785df4a4e21004beb/download/Camera.PNG "First Page Screen")

Settings Page
![Settings Page](https://trello.com/1/cards/61b664785829f94bb74e379d/attachments/623b6f4338f9ff32f595a71f/download/SettingsPage1.PNG "Settings Page 1")
![Settings Page 2](https://trello.com/1/cards/61b664785829f94bb74e379d/attachments/623b6f45245d1e39ace80a66/download/SettingsPage2.PNG "Settings Page 2")

Profile Page
![Profile Page](https://trello.com/1/cards/61b664785829f94bb74e379d/attachments/623b6f42e2f30a52b48a8e4c/download/ProfilePage.png "Profile Page" )


# Scrum process

## Sprint 0
Duration 10/01/22 - 18/01/22

### Completed Tasks:
		1. Software setup
		2. Trello setup
		3. README setup
		4. Contacted Company for update meeting
		5. Created Hello World application

### Contributions:
		1. Asia Solomianko: 20/20 points, 2 hours of teamwork, 2 commits
		2. Nathan Stull: 20/20 points, 2 hours of teamwork, 1 commit
		3. Katelyn Weidner: 20/20 points, 2 hours of teamwork, 1 commit
		4. Mick Ward: 20/20 points, 2 hours of teamwork, 1 commit  

## Sprint 1
Duration 18/01/22 - 25/01/22

### Completed Tasks:
		1. Set up basic framework
		2. Familiarized selves with react native and JavaScript
		
### Contributions:
		1. Asia Solomianko: 20/20 points, 4 hours teamwork, 1 commit
		2. Nathan Stull: 20/20 points, 4 hours teamwork, 2 commits
		3. Katie Weidner: 20/20, 4 hours teamwork, 4 commits
		4. Mick Ward: 20/20 points, 4 hours teamwork, 3 commits
		
## Sprint 2
Duration 25/01/22 - 01/02/22

### Completed Tasks:
		1. Met with Fred Dinkler for BFA database information
		2. Created login screen with hard coded credentials
		
### Contributions:
		1. Asia Solomianko: 20/20, 4 hours, 2 commits
		2. Nathan Stull: 20/20, 4 hours, 1 commit
		3. Katie Weidner: 20/20, 4 hours, 1 commit
		4. Mick Ward: 20/20, 4 hours, 2 commits

## Sprint 3
Duration 01/02/22 - 08/02/22

### Completed Tasks:
		1. Implemented login screen using BFA database
		2. Added comments to code snippets

### Contributions:
		1. Asia Solomianko: 20/20, 4 hours, 2 commits
		2. Nathan Stull: 20/20, 4 hours, 1 commit
		3. Katie Weidner: 20/20, 4 hours, 1 commit
		4. Mick Ward: 20/20, 4 hours, 4 commits
		
## Sprint 0-3 Retrospective
Our team has very opposite schedules. There is limtied time for us to meet, which has made working together difficult. However, we are very effective as a team when we meet together. A good thing we have done is set up weekly meetings with our sponsors. This way, we can have a blocked time in case we want to discuss anything. Something we can work on is staying on top of emails. We have a lot of back and forth on our emails, so creating new email threads to keep track of our conversations can increase the effectiveness of how we are sharing information.


## Sprint 4
Duration 09/02/22 - 15/02/22

### Completed Tasks:
		1. Login UI updated with pictures and visuals
		2. Added buttons on bottom after logging in (profile, camera, settings)
		
### Contributions:
		1. Asia Solomianko: 20/20 points, 4 hours teamwork, 6 commits
		2. Nathan Stull: 20/20 points, 4 hours teamwork, 1 commit
		3. Katie Weidner: 20/20, 4 hours teamwork, 8 commits
		4. Mick Ward: 20/20 points, 4 hours teamwork, 4 commits
		
## Sprint 5
Duration 16/02/22 - 23/02/22

### Completed Tasks:
		1. Added camera view on homepage
		2. Formatted camera
		3. Fixed homepage UI
		
### Contributions:
		1. Asia Solomianko: 20/20, 4 hours, 5 commits
		2. Nathan Stull: 20/20, 4 hours, 1 commit
		3. Katie Weidner: 20/20, 4 hours, 11 commits
		4. Mick Ward: 20/20, 4 hours, 2 commits

## Sprint 6
Duration 24/02/22 - 28/02/22

### Completed Tasks:
		1. Added settings buttons
		2. Timer buttons and timer functioning
		3. Crosshair
		4. Android UI adjustments
		5. Settings page UI started

### Contributions:
		1. Asia Solomianko: 20/20, 4 hours, 6 commits
		2. Nathan Stull: 20/20, 4 hours, 1 commit
		3. Katie Weidner: 20/20, 4 hours, 10 commits
		4. Mick Ward: 20/20, 4 hours, 3 commits
		
## Sprint 4-6 Retrospective
We work very well when working together, and we have made lots of progress meeting in the Capstone room. It is best to bounce ideas off of each other. We had very effective sprints, and weekly meetings with our company has been a helpful way to keep communication with them.


## Sprint 7
Duration 02/03/22 - 07/03/22

### Completed Tasks:
		1. Fixed Android app crashing issue
		2. Added buttons and other controls to Settings page

### Contributions:
		1. Asia Solomianko: 20/20, 4 hours, 1 commit
		2. Nathan Stull: 20/20, 4 hours, 0 commits
		3. Katie Weidner: 20/20, 4 hours, 3 commits
		4. Mick Ward: 20/20, 4 hours, 0 commits
		
		
## Sprint 8
Duration 08/03/22 - 11/03/22

### Completed Tasks:
		1. Adding settings UI
		2. Created profile page that has token on it
		3. Implementing timer circle

### Contributions:
		1. Asia Solomianko: 20/20, 4 hours, 7 commits
		2. Nathan Stull: 20/20, 4 hours, 1 commit
		3. Katie Weidner: 20/20, 4 hours, 4 commits
		4. Mick Ward: 20/20, 4 hours, 2 commits
		
		
## Sprint 9
Duration 21/03/22 - 28/03/22

### Completed Tasks:
		1. Working on getting an APK of old Android app

### Contributions:
		1. Asia Solomianko: 20/20, 6 hours
		2. Nathan Stull: 20/20, 6 hours
		3. Katie Weidner: 20/20, 6 hours
		4. Mick Ward: 20/20, 6 hours
		
## Sprint 7-9 Retrospective
As we are trying to get the UI finished, we needed to shift our efforts into creating an APK. Al requested that this is available for the BFA conference they're having the first week of April. Due to this, we stopped development. This isn't the most efficient, so for the next sprint we plan to split our groups into APK teams and dev teams. Katie, who is much more experienced doing UI, will get a lot more done continuing the UI rather than trying to figure out how to do the APK. Since none of us have experience with APKs and Android development, we are having a hard time progressing with this. Reaching out to someone with movile development, Dr. Bashias, has been helpful in progressing our efforts. We want to reach out to other people quicker now if we reach a roadblock.


## Sprint 10
Duration 29/03/22 - 04/04/22

### Completed Tasks:
		1. Get Android version up to date with iOS version
		2. Add table to home page for tracking measurements

### Contributions:
		1. Asia Solomianko: 20/20, 5 hours,  commit
		2. Nathan Stull: 20/20, 5 hours,  commits
		3. Katie Weidner: 20/20, 5 hours,  commits
		4. Mick Ward: 20/20, 5 hours,  commits
		
		
## Sprint 11
Duration 05/04/22 - 11/04/22

### Completed Tasks:
		1. Linking UI settings page buttons to 'back end' changes
		2. Record demo video for installing everything to run the project
		3. Created an app bundle for the Google Play store to allow user testing
		4. Created APK

### Contributions:
		1. Asia Solomianko: 20/20, 7 hours, commits
		2. Nathan Stull: 20/20, 7 hours,  commit
		3. Katie Weidner: 20/20, 7 hours,  commits
		4. Mick Ward: 20/20, 7 hours,  commits
		
		
## Sprint 12
Duration 12/04/22 - 26/04/22

### Completed Tasks:
		1. Expanding documentation and README
		2. Creating final presentation
		3. Finish settings page buttons to allow changing them

### Contributions:
		1. Asia Solomianko: 20/20, 6 hours,  commits
		2. Nathan Stull: 20/20, 6 hours,  commits
		3. Katie Weidner: 20/20, 6 hours,  commits
		4. Mick Ward: 20/20, 6 hours,  commits
		
## Sprint 10-12 Retrospective
Overall, we gained a lot of experience with this project. None of us previously have worked with mobile app development, and only half of us worked with React Native. We also learned that we work better when we meet up everyday. This helped us keep on track with our progress and update each other on any blockers we may have encountered.

# Company Support

We meet with the company sponsors once a week on Tuesdays at noon. This time is blocked off even if we do not need to discuss many matters.