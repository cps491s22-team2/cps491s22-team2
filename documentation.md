# Balloon App Documentation - How to Run the App


### The Team

Spring 2022

&nbsp;&nbsp;&nbsp;&nbsp;Asia Solomianko, <asia.solomianko@gmail.com>, 847-668-6252, Project Leader, worked with Google Play testing

&nbsp;&nbsp;&nbsp;&nbsp;Nathan Stull, <stulln994@gmail.com>, 937-260-3245, Android expert, worked with app bundle and APK building

&nbsp;&nbsp;&nbsp;&nbsp;Mick Ward, <22mickward@gmail.com>, 815-409-1435, React Native expert

&nbsp;&nbsp;&nbsp;&nbsp;Katie Weidner <weidnerkatie01@gmail.com>, 847-736-7637, UI expert


The four of us created this app from scratch. Here is information on how to clone the code onto your machine and any tips on how to run the app.


## Setting up the Environment

1. Pick a code editor

<ul>We all downloaded Visual Studio Code. The link to download VSCode is here: <https://code.visualstudio.com/>. Feel free to use another editor if preferred.</ul>

2. Clone the repo

<ul>We used React Native for our framework. In order to install this, you must run the `npm install -g expo-cli` command in the terminal. For more information on how to setup the environment, please refer to this link: <https://reactnative.dev/docs/environment-setup>.</ul>

3. Cd into the project

<ul>`cd BalloonApp`</ul>

4. Install Reactive Native

<ul>We used React Native for our framework. In order to install this, you must run the `npm install -g expo-cli` command in the terminal. This will install the Expo CLI command line utility. For more information on how to setup a React Native environment, please refer to this link: <https://reactnative.dev/docs/environment-setup>.</ul>

5. Install all packages and dependencies

<ul>`npm install`</ul>


## Packages that need to be downloaded inside BalloonApp directory:

All of the packages can be downloaded with the command `npm install`.

- `cd BalloonApp`
- `npm install`

Here are some of the packages we downloaded. This information is IF ONLY `npm install` does not work.
- npm install --global expo-cli
- expo install expo-camera
- npm install @react-navigation/native-stack
- npm install @react-navigation/native
- npm install react-native-table-component


## Possible errors:

1. There is a common error that comes up. We didn't catch what it is but when we Google it the solution is as follows: 
`npm install --save react-native-gesture-handler react-native-reanimated react-native-screens`


## How to Run the App

## Command to run it
- `expo start` 
- If that doesn't work, try `expo start --tunnel`
- Must download the Expo Go app on your phone to be able to scan the QR code
- On Android go into the Expo Go app and scan the QR code that is generated inside React Native
- On IOS go into your camera app and scan the QR code that is generated inside Raect Native

## Building the APK and App Bundle
Follow this website to help with building an APK or building the iOS app needed to publish on test flight <https://docs.expo.dev/classic/building-standalone-apps/>.
- To create an APK to be put directly on an Android device: `expo build:android -t apk`
- To create an app bundle to upload to the Google Play store: `expo build:android -t app-bundle`

## BFA video link showing the app in action
<https://vimeo.com/542837091>


## Postman
To access the BFA database, Fred Dinkler created an API on Postman. The files given to us by Fred can be found in the Downloads section on the left side of this Github repository. To download Postman, go to this link: <https://www.postman.com/downloads/>. Here is a tutorial on how to download and get Postman working: <https://www.guru99.com/postman-tutorial.html>.

The login workflow for the app goes like this:

* On launch, if there is no device name (something that the user called the device to identify it by), assume it's first launch. 
* Then, capture the device name and assign a unique device ID from the device's operating system. 
* On login, the authorized subscriptions and each expiration date is returned. If there are no primary plans, no app usage allowed. 
* The number and status of each device is also returned. = status of R = Registered, D = de-registered.

Important notes from Fred: 

* Pay attention to date/time formatting on the input data...dates can be specified as yyyy/mm/dd or yyyy-mm-dd however on return, the format will always be yyyy-mm-dd which is the database (MySQL) date-time format. I recommend sticking to the yyyy-mm-dd format for everything.
* The token returned on login uniquely identifies the user, so the user_id can generally be skipped in the input stream.
* In the table DDL (also attached) pay attention to fields with NOT NULL DEFAULT 'some value' - these can be missing from the input stream, but will assume the default value.
* There’s a lot of housekeeping data that you can ignore – these are demarcated by comments for clarity.
* Pay attention to fields with NOT NULL and no DEFAULT  value - these fields must be in the input stream or you'll throw a database error.
* Fields that are NULLable can be skipped if they don’t apply.
* The comments on each field in the DDL indicate valid values or ranges. If these edit parameters are wrong, or change, please let me know.
* Most numeric data is captured as text, since there's no data manipulation on the server side

Include the following in your UI:

* the ability to set a password on an observation set
* the ability to indicate if an observation set is publically available
* optionally, the date/time the observation set becomes visible to the public, and the date/time is stops being visible to the public (a non-owner user). NOTE: this feature allows for sharing the observation set (via the observation set ID) at an event, and supported by the api requesting a specific obsset by ID

Data to persist as app settings on the device:

* Username
* Email
* Password (think about hashing this)
* Token (not user modifiable, retrieved on each login)
* Device-id (not user modifiable)
* Host url (not user modifiable)  - dev.bfa.net for now,  bfa.net for live production


## Next steps: what we would've done if we had more time
* Putting information on the profile page (username, device name, any edit permissions, whatever else Al and Randy may like)
* Insert calculations for tracking balloon height
* Update table with measurements gotten
* Save measurements to the device and database
* Add hodograph
* Add android camera overlay to make it a circle since we couldn't achieve that with changing the code (we have a black circle uploaded, but there should be one to match the app's background color)
* Implement logout
* If you swipe to the right from the left screen, you get to the login page (we only want to go there if you logout, this may be a Stack Navigator feature that can't be changed, we don't know)
* Add/remove devices (there should only be 3 devices allowed to login per account)
* Make settings page UI more colorful (it's very gray right now)
* Change the color of the app to make it dark like the original
