-- table for sets of observations 
CREATE TABLE IF NOT EXISTS `#__windxplr_obsset` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'FK to Joomla User',
  `locationname` varchar(50) NOT NULL DEFAULT '<unnamed location>' COMMENT 'Freeform text',
  `rateclimbcoeff` char(3) DEFAULT NULL COMMENT 'Valid values: 000 to 900',
  `rateclimbuom` varchar(10) DEFAULT NULL COMMENT 'Valid values: ft/min, meters/min',
  `windspeeduom` varchar(5) NOT NULL DEFAULT 'mph' COMMENT 'Valid values: mph,kts,fps,mps,kph',
  `windspeeddir` char(4) NOT NULL DEFAULT 'To' COMMENT 'Valid values: To, From',
  `hododistanceuom` varchar(6) NOT NULL DEFAULT 'ft' COMMENT 'Valid values: ft, meters ',
  `heightinterval` char(3) NOT NULL DEFAULT '50' COMMENT 'Valid range: 25-800',
  `heightuom` varchar(6) NOT NULL DEFAULT 'ft' COMMENT 'Valid values: ft, meters ',
  `trackorientation` char(9) NOT NULL DEFAULT 'PORTRAIT' COMMENT 'Valid values: PORTRAIT,LANDSCAPE,SIDEWAYS',
  `meanfilterenabled` char(5) NOT NULL DEFAULT 'TRUE' COMMENT 'Valid values: TRUE,FALSE',
  `meanfiltertimeconst` char(3) NOT NULL DEFAULT '0.5' COMMENT 'Valid range: 0.0 - 5.0',
  `northtype` varchar(8) NOT NULL DEFAULT 'MAGNETIC' COMMENT 'Valid values: MAGNETIC,TRUE,GRID',
  `northoffset` char(4) NOT NULL DEFAULT '0' COMMENT 'Valid values: -360 to +360',
  `storeddatetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Date/time created',
  `lasteditdatetime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date/time last updated',
  `setpwd` varchar(20) DEFAULT NULL COMMENT 'Password to secure this set, not encrypted',
-- added version 1.8
  `is_public_yn` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Yes=1/No=0, if Yes, obsset is visible to the public',
  `public_start_dt` DATETIME NULL DEFAULT NULL COMMENT 'Date/time the obsset begins to be visible to the public',
  `public_stop_dt` DATETIME NULL DEFAULT NULL COMMENT 'Date/time the obsset stops being visible to the public' ,
-- added version 1.8
  `version` char(5) NOT NULL DEFAULT '1.8' COMMENT 'Product schema version',
--  Joomla component housekeeping data from here down
  `ordering` int(11) NOT NULL DEFAULT '0' COMMENT 'Joomla component data',
  `metakey` mediumtext COMMENT 'Joomla component data',
  `metadesc` mediumtext COMMENT 'Joomla component data',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Joomla component data',
  `hits` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Joomla component data',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` tinyint(3) NOT NULL DEFAULT '1',
  `metadata` mediumtext COMMENT 'Joomla component data',
  PRIMARY KEY (`id`),
  KEY `owner` (`user_id`),
  KEY `location` (`locationname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- obervations table
CREATE TABLE IF NOT EXISTS`#__windxplr_observations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `obsset_id` int(11) NOT NULL COMMENT 'FK to #_windxplr_obssets',
  `obsdatetime` datetime NOT NULL COMMENT 'Observation date/time',
  `height` varchar(8) NOT NULL COMMENT 'Height',
  `direction` char(8) NOT NULL COMMENT 'Direction',
  `speed` varchar(7) NOT NULL COMMENT 'Speed',
  `azimuth` varchar(7) NOT NULL COMMENT 'Azimuth',
  `elevation` varchar(7) NOT NULL,
  `avgspeed` varchar(7) NOT NULL,
--  Joomla component housekeeping data from here down
  `storeddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime entry created',
  `lasteditdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Datetime entry last updated',
  `ordering` int(11) NOT NULL DEFAULT '0' COMMENT 'Joomla component standard fields',
  `metakey` mediumtext COMMENT 'Joomla component standard fields',
  `metadesc` mediumtext COMMENT 'Joomla component standard fields',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Joomla component standard fields',
  `hits` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Joomla component standard fields',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` tinyint(3) NOT NULL DEFAULT '1',
  `metadata` mediumtext COMMENT 'Joomla component standard fields',
  PRIMARY KEY (`id`),
  KEY `obs_id` (`obsset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- new table v1.8
CREATE TABLE IF NOT EXISTS `#__windxplr_devices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'FK to Joomla user',
  `device_id` varchar(40) NOT NULL COMMENT 'Device ID provided from the device',
  `device_name` TINYTEXT NOT NULL COMMENT 'Device name - max 255',
  `registration_status` ENUM('R','D') NOT NULL DEFAULT 'D' COMMENT 'Device registration status, R=regsitered, D=deregistered',
  `registered_dt` datetime NULL COMMENT 'Datetime device was registered',
  `deregistered_dt` datetime NULL COMMENT 'Datetime device was deregisted',
--  Joomla component housekeeping data from here down
  `lastupdated_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Datetime entry last updated',
  `storeddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime entry created',
  `lasteditdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Datetime entry last updated',
  `version` char(5) NOT NULL DEFAULT '1.8' COMMENT 'Product schema version',
  `ordering` int(11) NOT NULL DEFAULT '0' COMMENT 'Joomla component standard fields',
  `metakey` mediumtext COMMENT 'Joomla component standard fields',
  `metadesc` mediumtext COMMENT 'Joomla component standard fields',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Joomla component standard fields',
  `hits` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Joomla component standard fields',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` tinyint(3) NOT NULL DEFAULT '1',
  `metadata` mediumtext COMMENT 'Joomla component standard fields',
  PRIMARY KEY (`id`),
  KEY `user_devices` (`user_id`),
  UNIQUE KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- new table v1.8
CREATE TABLE IF NOT EXISTS `#__windxplr_authplans` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cbsubs_plan_id` int(10) unsigned NOT NULL COMMENT 'FK to #__cbsubs_plans PK',
  `primary_yn` tinyint NOT NULL DEFAULT 0 COMMENT 'Primary plan? used to determine membetrship expiration',
  `qty_auth` TINYINT unsigned NOT NULL DEFAULT 1 COMMENT 'Number of concurrent registered apps allowed for this plan',
--  Joomla component housekeeping data from here down
  `lastupdated_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Datetime entry last updated',
  `storeddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime entry created',
  `lasteditdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Datetime entry last updated',
  `version` char(5) NOT NULL DEFAULT '1.8' COMMENT 'Product schema version',
  `ordering` int(11) NOT NULL DEFAULT '0' COMMENT 'Joomla component standard fields',
  `metakey` mediumtext COMMENT 'Joomla component standard fields',
  `metadesc` mediumtext COMMENT 'Joomla component standard fields',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Joomla component standard fields',
  `hits` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Joomla component standard fields',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` tinyint(3) NOT NULL DEFAULT '1',
  `metadata` mediumtext COMMENT 'Joomla component standard fields',
  PRIMARY KEY (`id`),
  UNIQUE KEY (`cbsubs_plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- default populating auth_plans data - v1.8
INSERT INTO `bfa32_windxplr_authplans`
(`cbsubs_plan_id`,`qty_auth`,`primary_yn`) 
VALUES
(79,1,1),
(59,1,1),
(60,1,1),
(61,1,1),
(55,1,1),
(76,1,1),
(56,1,1),
(57,1,1),
(70,1,0),
(82,1,0),
(71,1,0),
(83,1,0),
(80,1,0),
(85,1,0),
(66,1,0),
(22,1,0),
(81,1,0)
ON DUPLICATE KEY UPDATE `qty_auth`=VALUES(`qty_auth`)
;