import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {FirstPage } from './components/HomeScreen';
import { LoginScreen } from './components/LoginScreen';

// how we navigate between different pages
const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login" >
        <Stack.Screen name="Login" component={LoginScreen}  options={{headerShown: false}} screenOptions={{headerBackTitleVisible: false}}/>
        <Stack.Screen name="Home" component={FirstPage} options={{headerShown: false}}/> 
        {/* TODO: get rid of swipe left to go back to previous screen */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}