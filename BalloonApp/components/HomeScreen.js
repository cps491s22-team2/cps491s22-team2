import React from 'react';
import { StyleSheet, Text, View, Pressable, Image, Platform } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Profile } from './Profile';
import { Settings } from './Settings';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import { Camera } from 'expo-camera';
import {DeviceEventEmitter} from "react-native"
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { useState, useEffect } from 'react';
import { CurrentRenderContext } from '@react-navigation/native';
// import CircularProgress from 'react-native-circular-progress-indicator';

// -------- FIRST PAGE SCREEN --------
export function HomeScreen({ navigation }) {

  const initialSettingsState = {
    MSLOrAGL: 0, // 0 is MSL, 1 is AGL
    fieldElevationMSL: "0",
    fieldElevationMSLUnit: 0, //0 is feet, 1 is meters
    fieldElevationAGL: "0",
    fieldElevationAGLUnit: "feet",
    rateOfClimbCoefficient: "330",
    rateOfClimbUnit: 0, //0 is feet/min, 1 is feet/sec, 2 is meters/min, 3 is meters/sec
    measurementHeightInterval: "100",
    measurementHeightIntervalUnit: 0, //0 is feet, 1 is meters
    windspeedUnit: 0, // 0 is miles/hr, 1 is km/hr, 2 is knots, 3 is ft/min, 4 is m/min
    directionReporting: 0, // 0 is To, 1 is From
    distanceTraveledunit: 0, // 0 is feet, 1 is miles, 2 is meters, 3 is km, 4 is Naut. Ms
    sensorSamplingPeriod: 50,
    autoFocusTimeout: "0"
  };

  const [settingsState, setSettingsState] = useState(initialSettingsState);


  const [hasPermission, setHasPermission] = useState(null);
  const [isStopwatchStart, setIsStopwatchStart] = useState(false);
  const [time, setTime] = useState(0);
  const [intervalId, setIntervalId] = useState(0);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  DeviceEventEmitter.addListener("settingsUpdate", (eventData) => 
    setSettingsState(eventData));

  // when clicking the settings button, it should navigate to the next page
  settingsClick = async () => {
    navigation.navigate('Settings', {initialSettingsState: settingsState})
  }

  homeClick = async () => {
    navigation.navigate('HomeScreen')
  }

  profileClick = async () => {
    navigation.navigate('Profile')
  }

  if (hasPermission === null) {
    return <View />;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  const handleTimeStart = () => {
    setIsStopwatchStart(true)
    const newIntervalId = setInterval(() => {
      setTime(prevCount => prevCount + 1);
    }, 1000);

    setIntervalId(newIntervalId);
  }

  const handleTimeStop = () => {
    setIsStopwatchStart(false);
    clearInterval(intervalId);
    setTime(0);
  }

  const state = {
    tableHeader: ['Height (ft)', 'Direction (To)', 'Speed (mph)'],
    tableData: []
  }

  // what is being rendered on the page
  return (
    <View style={styles.container}>

        <Text style={styles.time}>Time</Text>
        <Text style={styles.timeText}>{time + "s"}</Text>


        <Text style={styles.height}>Height</Text>
        <Text style={styles.heightText}>0ft</Text>

        <Camera style={styles.cameraView} type={Camera.Constants.Type.back}>
          <Feather name="crosshair" size = {30} color='red' style={styles.crosshair}/>
          <View style={styles.timeProgress}>
            {/* <CircularProgress
              value={100}
              radius={183}
              duration={2000}
              textColor={'transparent'}
              maxValue={100}
              titleColor={'transparent'}
              inActiveStrokeColor={'#fff'}
              activeStrokeColor={'#8B0000'}
              ActiveStrokeOpacity={0.2}
              inActiveStrokeWidth={12}
              activeStrokeWidth={12}
            /> */}
          </View>
        </Camera>
        
        {/* This code below will add the android cutout camera */}
        {/* {Platform.OS === 'android' ? <Image source={require('./camera_circle_cutout.png')} resizeMode={"cover"} style={styles.cameraCutout}></Image>: null } */}


        <Table style={styles.tableView} borderStyle={{borderWidth: 1, borderColor: '#ffa1d2'}}>
          <Row data={state.tableHeader} style={styles.tableHeader} textStyle={styles.headerText}/>
          <TableWrapper style={styles.wrapper}>
            <Rows data={state.tableData} style={styles.row} textStyle={styles.tableText}/>
          </TableWrapper>
        </Table>


        <Pressable style={styles.invalidateButton} onPressOut={() => console.log(settingsState)}>
          <Text style={styles.invalidateButtonText}>Invalidate Last {'\n'} Measurement</Text>
        </Pressable>

        <Pressable style={isStopwatchStart ? styles.stopButton : styles.startButton} onPressOut={() => isStopwatchStart ? handleTimeStop() : handleTimeStart()}>
          <Text style={styles.startButtonText}>{isStopwatchStart ? "Stop" : "Start"}</Text>
        </Pressable>

        {/* code for navigation bar at the bottom */}
        <View style={styles.navigationContainer}>
          <Ionicons name='ios-person' size={30} color='black' onPress={() => profileClick()} style={styles.profile} />
          <Ionicons name='md-home' size={30} color='black' onPress={() => homeClick()} style={styles.homeIcon}/>
          <Ionicons name='ios-settings' size={30} color='black' onPress={() => settingsClick()} style={styles.settings}/>   
        </View>

    </View>
  );
} 

const Stack = createNativeStackNavigator();

export function FirstPage() {
  return (
    <Stack.Navigator initialRouteName="HomeScreen" screenOptions={{ headerShown: false }} >
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="Settings" component={Settings} />
      <Stack.Screen name="Profile" component={Profile} />
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: '15%'
  },
  tableContainer: { 
    flex: 1, 
    padding: 16, 
    paddingTop: 30, 
    backgroundColor: '#fff',
  },
  navigationContainer: {
    flex: 1,
    flexDirection: "row",
    marginBottom: '-5%',
    ...Platform.select( {
      android: {
        bottom: '82%',
        marginBottom: '0%'
      }
    })
  },
  profile: {
    // add styling as needed
    ...Platform.select( {
      android: {
        marginTop: '-25%'
      }
    })
  },
  settings: {
    ...Platform.select( {
      android: {
        marginTop: '-25%'
      }
    })
  },
  time: {
    color: 'dimgray',
    textAlign: 'left',
    right: '42%',
    top: '1%',
    fontSize: 16,
    ...Platform.select( {
      android: {
        top: '48%',
      }
    })
  },
  timeText: {
    color: 'dimgray',
    textAlign: 'left',
    right: '42%',
    top: '1%',
    fontSize: 16,
    ...Platform.select( {
      android: {
        marginBottom: '10%',
        top: '48%',
      }
    })
  },
  height: {
    color: 'dimgray',
    top: '-4.5%',
    left: '40%',
    fontSize: 16,
    ...Platform.select( {
      android: {
        top: '37%',
      }
    })
  },
  heightText: {
    color: 'dimgray',
    fontSize: 16,
    top: '-4.5%',
    left: '40%',
    ...Platform.select( {
      android: {
        marginBottom: '10%',
        top: '37%',
      }
    })
  },
  homeIcon: {
    paddingLeft: '15%',
    paddingRight: '15%',
    ...Platform.select( {
      android: {
        marginTop: '-25%'
      }
    })
  },
  cameraView: {
    borderRadius: 170,
    overflow: 'hidden',
    paddingVertical: 50,
    height: 350,
    width: 350,
    ...Platform.select( {
      ios: {
        marginTop: '-10%',
        marginBottom: '30%'
      },
      android: {
        marginTop: '60%',
        marginBottom: '5%'
      }
    })
  },
  timeProgress: {
    top: '-35.5%',
    left: '-2.5%'
  },
  crosshair: {
    top: '45%',
    left: '45%'
  },
  invalidateButton: {
    paddingVertical: 8,
    paddingHorizontal: 8,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: '#0096FF',
    left: '-20%',
    ...Platform.select( {
      ios: {
        marginBottom: '10%',
      },
      android: {
        marginBottom: '110%',
      }
    })
  },
  invalidateButtonText: {
    color: 'white'
  },
  startButton: {
    paddingVertical: 16,
    paddingHorizontal: 40,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: '#50C878',
    marginBottom: '10%',
    marginTop: '-23%',
    right: '-20%',
    ...Platform.select( {
      android: {
        marginBottom: '110%',
        marginTop: '-122%',
      }
    })
  },
  startButtonText: {
    color: 'white'
  },
  stopButton: {
    paddingVertical: 16,
    paddingHorizontal: 40,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: '#FF0000',
    marginBottom: '10%',
    marginTop: '-23%',
    right: '-20%',
  },
  stopButtonText: {
    color: 'white'
  },
  tableView: {
    ...Platform.select( {
      ios: {
        marginBottom: '45%',
        marginTop: '-25%',
      },
      android: {
        marginBottom: '45%', 
      }
    })
  },
  tableHeader: { 
    height: 40,
    width: 375, 
    backgroundColor: '#f1f8ff',
  },
  headerText: {
    textAlign: 'center',
  },
  tableText: { 
    margin: 6,
  },
  // wrapper: { 
  //   flexDirection: 'row',
  // },
});