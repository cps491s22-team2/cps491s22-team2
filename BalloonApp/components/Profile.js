import React, { useState } from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { AsyncStorage } from 'react-native';

export function Profile({ navigation }) {

    const [authToken, setAuthToken] = useState("");

    retrieveAuthToken = async () => {
        try {
          const value = await AsyncStorage.getItem('authToken');
          if (value !== null) {
            console.log(value);
            setAuthToken(value);
          }
        } catch (error) {
          // Error retrieving data
        }
      };
    
    retrieveAuthToken();

    // when clicking the home button, it should navigate back to the home page
    homeClick = async () => {
      navigation.navigate('HomeScreen')
    }

    return (
        <View style={styles.container}>

            <View style={styles.container}>
                <Text>Profile Page</Text>
                <Text style={styles.token}>{"Token: " + authToken}</Text> 
            </View>


            <View style={styles.navigationContainer}>
                <Ionicons name='md-home' size={30} color='black' onPress={() => homeClick()} style={styles.homeIcon}/>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    navigationContainer: {
      flex: 1,
      flexDirection: "row",
      marginTop: '5%',
      marginBottom: '-162%',
      ...Platform.select( {
        ios: {
          marginBottom: '-185%',
        }
      })
    },
    homeIcon: {
      paddingLeft: '15%',
      paddingRight: '15%'
    }
  });