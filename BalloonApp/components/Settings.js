import React, { useState, useEffect } from 'react';
import {StyleSheet, Text, View, Switch, Button} from 'react-native';
import { AsyncStorage } from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SegmentedControl from '@react-native-segmented-control/segmented-control';
import SegmentedControlTab from "react-native-segmented-control-tab";
import UselessTextInput from './TextInput';
import {DeviceEventEmitter} from "react-native"

export function Settings({ route, navigation }) {

  const {initialSettingsState} = route.params;

  const [authToken, setAuthToken] = useState("");
  const [tempSettingsState, setTempSettingsState] = useState(initialSettingsState);
  const [isEnabledAutoFocus, setIsEnabledAutoFocus] = useState(false);
  const [isEnabledAudio, setIsEnabledAudio] = useState(false);
  //comment to check if i can push/pull from bitbuckety

  const toggleSwitchAutoFocus = () => setIsEnabledAutoFocus(previousState => !previousState);
  const toggleSwitchAudio = () => setIsEnabledAudio(previousState => !previousState);

  const [state, setState] = useState(0);

  // when clicking the home button, it should navigate back to the home page
  homeClick = async () => {
    navigation.navigate('HomeScreen')
  }

  //Text input field methods
  updateFieldElevation = () => {}

  updateMSL = () => {
   console.log(tempSettingsState)
  }



  updateRateOfClimbCoEff = () => {

  }

  updateAutofocusTimeout = () => {

  }

  profileClick = async () => {
    navigation.navigate('Profile')
  }

  /*segementedcontroltab for android code below: */

  const [selectedIndices, setSelectedIndices] = useState([0]);


  const handleMultipleIndexSelect = (index) => {
    // For multi Tab Selection SegmentedControlTab
    if (selectedIndices.includes(index)) {
      // Included in the selected array then remove
      setSelectedIndices(
        selectedIndices.filter((i) => i !== index)
      );
    } else {
      // Not included in the selected array then add
      setSelectedIndices([...selectedIndices, index]);
    }
  };

  /* end android segementedcontroltab methods */

  saveSettings = () => {
    DeviceEventEmitter.emit("settingsUpdate", tempSettingsState);
  }


  return (
    <View style={styles.container}>
      
      {Platform.OS == "ios" && 
        // <Button title='save' color='black' onPress={() => saveSettings()} styles={styles.saveButtonIOS}></Button>
        <TouchableOpacity onPress={saveSettings()}>
            <View style={styles.saveButtonIOS}>
                <Text style={styles.saveButtonTextIOS}>save</Text>
            </View>
        </TouchableOpacity>
      }

      {Platform.OS == "android" && 
        // <Button title='save' color='transparent' onPress={() => saveSettings()} styles={styles.saveButtonAndroid}></Button>
        <TouchableOpacity onPress={saveSettings()}>
            <View style={styles.saveButtonAndroid}>
                <Text style={styles.saveButtonTextAndroid}>save</Text>
            </View>
        </TouchableOpacity>
      }

      <ScrollView style={styles.scrollView}>
{/* ------------- FIELD ELEVATION SECTION -------------*/}
          <Text style={styles.title_header}>FIELD ELEVATION</Text>

          <TouchableOpacity disabled={true} key="MSL or AGL">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>MSL or AGL</Text>
                <Text style={styles.subtitle}>Select field elevation </Text>
               
                {/* CHANGES WHAT TYPE OF SEGMENTED CONTROL BASED ON IOS OR ANDROID - same for all below as well */}

                {Platform.OS == "ios" && 
                  <SegmentedControl
                  values={['MSL', 'AGL']}
                  onChange={(event => {
                    setTempSettingsState({...tempSettingsState, MSLOrAGL: event.nativeEvent.selectedSegmentIndex})
                  })}
                  selectedIndex={tempSettingsState.MSLOrAGL}
                  paddingVertical={6}
                  tabsContainerStyle={styles.segmentedControl}
                  /> 
                }
                
                {Platform.OS == "android" && 
                  <SegmentedControlTab
                    values={['MSL', 'AGL']}
                    onChange={(event => {
                      setTempSettingsState({...tempSettingsState, MSLOrAGL: event.nativeEvent.selectedSegmentIndex})
                    })}
                    selectedIndex={tempSettingsState.MSLOrAGL}
                    // tabStyle={styles.tabStyle}
                    activeTabStyle={styles.activeTabStyle}
                    backgroundColor="lightgrey"
                    tabStyle={styles.tabStyle}
                    tabTextStyle={styles.tabTextStyle}
                    activeTabTextStyle={styles.activeTabTextStyle}
                  />
                }

                {/* -------------------------------------------------------------- */}

            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>
          <TouchableOpacity disabled={true} key="Field Elevation MSL">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Field Elevation MSL</Text>
                <Text style={styles.subtitle}>Set Field Elevation MSL (leave empty for GPS-based)</Text>
                <UselessTextInput password={false} 
                  inputValue={tempSettingsState.fieldElevationMSL} 
                  keyboardType={"number-pad"} 
                  onTextChange={(value => {
                      setTempSettingsState({...tempSettingsState, fieldElevationMSL: value})
                    })} 
                  styles={styles.textInput} 
                  placeHolder={"000"}>

                </UselessTextInput>
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>
          <TouchableOpacity disabled={true} key="Field Elevation MSL Unit">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Field Elevation MSL Unit</Text>
                <Text style={styles.subtitle}>Select field elevation MSL unit</Text>
                
                {Platform.OS == "ios" && 
                  <SegmentedControl
                    values={['feet', 'meters']}
                    selectedIndex={tempSettingsState.fieldElevationMSLUnit}
                    onChange={(event => {
                      setTempSettingsState({...tempSettingsState, fieldElevationMSLUnit: event.nativeEvent.selectedSegmentIndex})
                    })}
                    paddingVertical={6}
                    style={styles.segmentedControl}
                  />     
                }

                {Platform.OS == "android" && 
                  <SegmentedControlTab
                    values={['feet', 'meters']}
                    selectedIndex={tempSettingsState.fieldElevationMSLUnit}
                    onChange={(event => {
                      setTempSettingsState({...tempSettingsState, fieldElevationMSLUnit: event.nativeEvent.selectedSegmentIndex})
                    })}
                    backgroundColor="lightgrey"
                    tabStyle={styles.tabStyle}
                    tabTextStyle={styles.tabTextStyle}
                    activeTabStyle={styles.activeTabStyle}
                    activeTabTextStyle={styles.activeTabTextStyle}
                  />
                }
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>

{/* ------------- BALLOON CLIMB SECTION ------------- */}
        <Text style={styles.title_header}>BALLOON CLIMB</Text>
        <TouchableOpacity disabled={true} key="Rate of Climb Coefficient">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Rate of Climb Coefficient</Text>
                <Text style={styles.subtitle}>Set Coefficient</Text>
                <UselessTextInput 
                  password={false} 
                  inputValue={tempSettingsState.rateOfClimbCoefficient} 
                  keyboardType={"number-pad"} 
                  onTextChange={(value => {
                    setTempSettingsState({...tempSettingsState, rateOfClimbCoefficient: value})
                  })} 
                  styles={styles.textInput} 
                  placeHolder={"350"} 
                  placeholderTextColor="0000FF" >
                </UselessTextInput>
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>
          <TouchableOpacity disabled={true} key="Rate of Climb Unit">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Rate of Climb Unit</Text>
                <Text style={styles.subtitle}>Select coefficient unit of measure</Text>

                {Platform.OS == "ios" && 
                  <SegmentedControl
                    values={['feet/min', 'feet/sec','meters/min','meters/sec']}
                    selectedIndex={tempSettingsState.rateOfClimbUnit}
                    //selectedIndex={this.state.selectedIndex}
                    onChange={(event => {
                      setTempSettingsState({...tempSettingsState, rateOfClimbUnit: event.nativeEvent.selectedSegmentIndex})
                    })}
                    paddingVertical={6}
                    style={styles.segmentedControl}
                  />     
                } 

                {Platform.OS == "android" && 
                  <SegmentedControlTab
                    values={['feet/min', 'feet/sec','meters/min','meters/sec']}
                    selectedIndex={tempSettingsState.rateOfClimbUnit}
                    onChange={(event => {
                      setTempSettingsState({...tempSettingsState, rateOfClimbUnit: event.nativeEvent.selectedSegmentIndex})
                    })}
                    backgroundColor="lightgrey"
                    tabStyle={styles.tabStyle}
                    tabTextStyle={styles.tabTextStyle}
                    activeTabStyle={styles.activeTabStyle}
                    activeTabTextStyle={styles.activeTabTextStyle}
                  />
                }

            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>
          <TouchableOpacity disabled={true} key="Measurement Height Interval">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Measurement Height Interval</Text>
                <Text style={styles.subtitle}>Set Measurement Height Interval</Text>
                
                {Platform.OS == "ios" && 
                  <SegmentedControl
                    values={['feet', 'meters']}
                    selectedIndex={tempSettingsState.measurementHeightIntervalUnit}
                    //selectedIndex={this.state.selectedIndex}
                    onChange={((event => {
                      setTempSettingsState({...tempSettingsState, measurementHeightIntervalUnit: event.nativeEvent.selectedSegmentIndex})
                    }))}
                    paddingVertical={6}
                    style={styles.segmentedControl}
                  />   
                }

                {Platform.OS == "android" && 
                  <SegmentedControlTab
                    values={['feet', 'meters']}
                    selectedIndex={tempSettingsState.measurementHeightIntervalUnit}
                    onChange={((event => {
                      setTempSettingsState({...tempSettingsState, measurementHeightIntervalUnit: event.nativeEvent.selectedSegmentIndex})
                    }))}
                    backgroundColor="lightgrey"
                    tabStyle={styles.tabStyle}
                    tabTextStyle={styles.tabTextStyle}
                    activeTabStyle={styles.activeTabStyle}
                    activeTabTextStyle={styles.activeTabTextStyle}
                  />
                }
              
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>

{/* ------------- WINDS SECTION ------------- */}
        <Text style={styles.title_header}>WINDS</Text>
        <TouchableOpacity disabled={true} key="Windspeed Unit">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Windspeed Unit</Text>
                <Text style={styles.subtitle}>Select windspeed unit of measure</Text>
                
                {Platform.OS == "ios" && 
                  <SegmentedControl
                    values={['miles/hr', 'km/hr', 'knots', 'ft/min', 'm/min']}
                    selectedIndex={tempSettingsState.windspeedUnit}
                    onChange={((event => {
                      setTempSettingsState({...tempSettingsState, windspeedUnit: event.nativeEvent.selectedSegmentIndex})
                    }))}
                    paddingVertical={6}
                    style={styles.segmentedControl}
                  />  
                }

                {Platform.OS == "android" && 
                  <SegmentedControlTab
                    values={['miles/hr', 'km/hr', 'knots', 'ft/min', 'm/min']}
                    selectedIndex={tempSettingsState.windspeedUnit}
                    onChange={((event => {
                      setTempSettingsState({...tempSettingsState, windspeedUnit: event.nativeEvent.selectedSegmentIndex})
                    }))}
                    backgroundColor="lightgrey"
                    tabStyle={styles.tabStyle}
                    tabTextStyle={styles.tabTextStyle}
                    activeTabStyle={styles.activeTabStyle}
                    activeTabTextStyle={styles.activeTabTextStyle}
                  />
                } 

            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>
          <TouchableOpacity disabled={true} key="Direction Reporting">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Direction Reporting</Text>
                <Text style={styles.subtitle}>Blowing To/From</Text>

                {Platform.OS == "ios" && 
                  <SegmentedControl
                    values={['To', 'From']}
                    selectedIndex={tempSettingsState.directionReporting}
                    //selectedIndex={this.state.selectedIndex}
                    onChange={((event => {
                      setTempSettingsState({...tempSettingsState, directionReporting: event.nativeEvent.selectedSegmentIndex})
                    }))}
                    paddingVertical={6}
                    style={styles.segmentedControl}
                  />
                } 

                {Platform.OS == "android" && 
                  <SegmentedControlTab
                    values={['To', 'From']}
                    selectedIndex={tempSettingsState.directionReporting}
                    onChange={((event => {
                      setTempSettingsState({...tempSettingsState, directionReporting: event.nativeEvent.selectedSegmentIndex})
                    }))}
                    backgroundColor="lightgrey"
                    tabStyle={styles.tabStyle}
                    tabTextStyle={styles.tabTextStyle}
                    activeTabStyle={styles.activeTabStyle}
                    activeTabTextStyle={styles.activeTabTextStyle}
                  />
                }
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>
          <TouchableOpacity disabled={true} key="Distance Traveled Unit">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Distance Traveled Unit</Text>
                <Text style={styles.subtitle}>Select unit of measure for distance traveled by winds for each vector</Text>

                {Platform.OS == "ios" && 
                  <SegmentedControl
                    values={['Ft', 'Mi', 'Meters', 'Km', 'Naut. M']}
                    selectedIndex={0}
                    //selectedIndex={this.state.selectedIndex}
                    onChange={() => {}}
                    paddingVertical={6}
                    style={styles.segmentedControl}
                  />  
                }

                {Platform.OS == "android" && 
                  <SegmentedControlTab
                    values={['Ft', 'Mi', 'Meters', 'Km', 'Naut. M']}
                    multiple
                    selectedIndices={selectedIndices}
                    onTabPress={handleMultipleIndexSelect}
                    backgroundColor="lightgrey"
                    tabStyle={styles.tabStyle}
                    tabTextStyle={styles.tabTextStyle}
                    activeTabStyle={styles.activeTabStyle}
                    activeTabTextStyle={styles.activeTabTextStyle}
                  />
                } 
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>


{/* ------------- TRACKING AIDES SECTION ------------- */}
        <Text style={styles.title_header}>TRACKING AIDES</Text>
        <TouchableOpacity disabled={true} key="Tracking Orientation">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Tracking Orientation</Text>
                <Text style={styles.subtitle}>Select which phone orientation you would like to use to track the balloon</Text>

                {Platform.OS == "ios" && 
                  <SegmentedControl
                    values={['Portrait', 'Landscape', 'Sideways']}
                    selectedIndex={0}
                    //selectedIndex={this.state.selectedIndex}
                    onChange={() => { }}
                    paddingVertical={6}
                    style={styles.segmentedControl}
                  />  
                }

                {Platform.OS == "android" && 
                  <SegmentedControlTab
                    values={['Portrait', 'Landscape', 'Sideways']}
                    multiple
                    selectedIndices={selectedIndices}
                    onTabPress={handleMultipleIndexSelect}
                    backgroundColor="lightgrey"
                    tabStyle={styles.tabStyle}
                    tabTextStyle={styles.tabTextStyle}
                    activeTabStyle={styles.activeTabStyle}
                    activeTabTextStyle={styles.activeTabTextStyle}
                  />
                }

            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>
          <TouchableOpacity disabled={true} key="Enable Audio Warning">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Enable Audio Warning</Text>
                <Text style={styles.subtitle}>Turn audio warning before samples On/Off</Text>
                <Switch
                  trackColor={{ false: "#D9DADC", true: "#4ED164" }}
                  thumbColor={isEnabledAudio ? "#fff" : "#fff"}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={toggleSwitchAudio}
                  value={isEnabledAudio}
                  style={styles.switch}
                />
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>
          <TouchableOpacity disabled={true} key="Camera Autofocus Timeout">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Camera Autofocus Timeout</Text>
                <Text style={styles.subtitle}>Turn Autofocus timeout On/Off</Text>
                <Switch
                  trackColor={{ false: "#D9DADC", true: "#4ED164" }}
                  thumbColor={isEnabledAutoFocus ? "#fff" : "#fff"}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={toggleSwitchAutoFocus}
                  value={isEnabledAutoFocus}
                  style={styles.switch}
                />
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>
          <TouchableOpacity disabled={true} key="Autofocus Timeout">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Autofocus Timeout</Text>
                <Text style={styles.subtitle}>Set camera focus to infinity after time (seconds)</Text>
                <UselessTextInput 
                  password={false} 
                  inputValue={tempSettingsState.autoFocusTimeout}
                  keyboardType={"number-pad"} 
                  onTextChange={(value => {
                    setTempSettingsState({...tempSettingsState, autoFocusTimeout: value})
                  })} 
                  styles={styles.textInput} 
                  placeHolder={"50"}>

                </UselessTextInput>
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
          </TouchableOpacity>

{/* ------------- NORTH SECTION ------------- */}
        <Text style={styles.title_header}>NORTH</Text>
        <TouchableOpacity disabled={true} key="Type of North">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Type of North</Text>
                <Text style={styles.subtitle}></Text>

                {Platform.OS == "ios" && 
                  <SegmentedControl
                    values={['Magnetic', 'True', 'Grid']}
                    selectedIndex={0}
                    //selectedIndex={this.state.selectedIndex}
                    onChange={() => { }}
                    paddingVertical={6}
                    style={styles.segmentedControl}
                  />
                }

                {Platform.OS == "android" && 
                  <SegmentedControlTab
                    values={['Magnetic', 'True', 'Grid']}
                    multiple
                    selectedIndices={selectedIndices}
                    onTabPress={handleMultipleIndexSelect}
                    backgroundColor="lightgrey"
                    tabStyle={styles.tabStyle}
                    tabTextStyle={styles.tabTextStyle}
                    activeTabStyle={styles.activeTabStyle}
                    activeTabTextStyle={styles.activeTabTextStyle}
                  />
                }
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
        </TouchableOpacity>

{/* ------------- ADDITIONAL SETTINGS SECTION ------------- */}
        <Text style={styles.title_header}>ADDITIONAL SETTINGS</Text>
        <TouchableOpacity disabled={true} key="Advanced Settings">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>Advanced Settings</Text>
                <Text style={styles.subtitle}></Text>
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
        </TouchableOpacity>
        <TouchableOpacity disabled={true} key="About">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>About</Text>
              {/* EASTER EGG HIDDEN HERE ?? */}
                <Text style={styles.subtitle}>This application was developed by students at the University of Dayton for the Balloon Federation of America</Text>
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
        </TouchableOpacity>
        <TouchableOpacity disabled={true} key="License Expiration Date and Time">
            <View style={styles.header}>
              <Text style={{fontSize: 17}}>License Expiration Date and Time</Text>
                <Text style={styles.subtitle}>Nov 25, 2023</Text>
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
        </TouchableOpacity>

        {/* ------------- LOGOUT SECTION ------------- */}
        <Text style={styles.title_header}>LOGOUT</Text>
        <TouchableOpacity key="Logout">
            <View style={styles.logout}>
              <Text style={styles.subtitle}></Text>
              <Text style={{fontSize: 20}}>Logout</Text>
              <Text style={styles.subtitle}></Text>
            </View>
            <View style={{height: 0.5, backgroundColor: 'grey'}} />
        </TouchableOpacity>
      </ScrollView>

      <View style={styles.navigationContainer}>
        <Ionicons name='md-home' size={30} color='black' onPress={() => homeClick()} style={styles.homeIcon}/>
      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '12%',
    marginBottom: '2%'
  },
  saveButtonIOS: {    
    color: 'white',
    paddingBottom: '-5%',
    // marginTop: '2%'
  },
  saveButtonTextIOS: {
    color: 'black',
    fontSize: 16
  },
  saveButtonAndroid: {
    color: 'white',
  },
  saveButtonTextAndroid: {
    color: 'black',
    fontSize: 18
  },
  scrollView: {
    flex: 5, 
    backgroundColor: 'white',
    width: '100%'
  },
  header: {
    paddingBottom: 5,
    paddingTop: 10,
    paddingLeft: 10,
    backgroundColor: 'lightgrey',
  },
  logout: {
    paddingBottom: 15,
    paddingTop: 10,
    backgroundColor: 'lightgrey',
    alignItems: 'center'
  },
  title_header: {
    paddingHorizontal: 20,
    paddingBottom: 20,
    paddingTop: 20,
    paddingLeft: 5,
    fontWeight: 'bold',
  },
  subtitle: {
    fontSize: 12, 
    opacity: 0.5, 
    paddingTop: 5,
    paddingRight: 5
  },
  navigationContainer: {
    flex: 1,
    flexDirection: "row",
    marginTop: '5%',
    marginBottom: '-162%',
    ...Platform.select( {
      android: {
        marginBottom: '-148%',
        paddingBottom: '5%'
      }
    })
  },
  switch: {
    marginTop: '-9%',
    marginBottom: '2%',
    marginLeft: '77%'
  },
  segmentedControl: {
    marginRight: '2%',
    marginTop: '1%'
  },
  tabStyle: {
    borderColor: 'lightgrey',
    textDecorationColor: 'black',
    backgroundColor: 'lightgrey',
    borderWidth: 0,
    borderColor: 'transparent'
  },
  tabTextStyle: {
    color: 'black'
  },
  activeTabStyle: {
    backgroundColor: 'white'
  },
  activeTabTextStyle: {
    color: 'black'
  },
  textInput: {
    width: '97%',
    backgroundColor: 'darkgrey',
    borderRadius: 4,
    marginTop: '2%',
    marginBottom: '2%',
    paddingBottom: '5%',
    paddingTop: '5%',
    textAlign: 'center',
    color: '#fff'
  },
  homeIcon: {
    paddingLeft: '15%',
    paddingRight: '15%',
  }
});