import React from "react";
import { ProgressViewIOSComponent, SafeAreaView, StyleSheet, TextInput} from "react-native";

const UselessTextInput = (props) => {
  
  const {onTextChange, inputValue} = props;
  
  return (
    <SafeAreaView>
      <TextInput
        style={props.styles}
        onChangeText={onTextChange}
        value={inputValue}
        secureTextEntry={props.password}
        placeholder={props.placeHolder}
        placeholderTextColor={"white"}
        keyboardType={props.keyboardType || "default"}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});

export default UselessTextInput;