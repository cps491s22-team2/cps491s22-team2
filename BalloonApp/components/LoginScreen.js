import { StatusBar } from 'expo-status-bar';
import React, {useState, Component } from 'react';
import { Button, StyleSheet, Text, KeyboardAvoidingView, Alert, ImageBackground, View, Image, TextInput} from 'react-native';
import UselessTextInput from './TextInput';
import { AsyncStorage } from 'react-native';

// -------- LOGIN SCREEN --------
export function LoginScreen({ navigation }) { 
    const [loginInfo, setLoginInfo] = useState({username: "", password: ""})
  
    // update the username variable for the username inputted through text
    updateUsername = (username) => {
      setLoginInfo({username: username, password: loginInfo.password})
    }
  
    // update the password variable for the password inputted through text
    updatePassword = (password) => {
      setLoginInfo({username: loginInfo.username, password: password})
    }

    storeAuthToken = async (token) => {
      try {
        await AsyncStorage.setItem(
          'authToken',
          token
        );
      } catch (error) {
        // Error saving data
      }
    };
  
    loginClick = async (username, password) => {
      var LoginResponse = null;
  
      // sending username and password from login screen to BFA server using this API
      try {
        const response = await fetch('http://dev.bfa.net/windapi/windxplr/login', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            "username":username,
            "password":password,
            "email":"fred+testuser00@dinkler.com"
            })
        });
  
        LoginResponse = await response.json();
  
      } catch (error) {
        console.error(error);
      }
  
      console.log("Username and password > " + username + ":" + password)

      // make sure login is successful with a 200 response code
      // TODO: connect subscriptions with login too
      if(LoginResponse && LoginResponse.data.rc === "200" && LoginResponse.data.authplans_count > 0){
        storeAuthToken( LoginResponse.data.token)
        navigation.navigate('Home')
      } else {
        Alert.alert(
          "Unsuccessful login",
          "Please try to login again.",
          [
            { text: "Ok", onPress: () => console.log("Ok Pressed") }
          ]
        );
      }
    }

    // what is rendered on the page
    return (
      
    <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={styles.container}>
      <ImageBackground style={styles.container} source={require('./BackgroundBlurred.png')}>
        <View>

          <Image source={require('./BFA_logo.png')} resizeMode={"cover"} style={styles.logo}/>
          <Text style={styles.appName}>Wind Explorer</Text>
          <UselessTextInput password={false} inputValue={loginInfo.username} onTextChange={updateUsername} styles={styles.loginInput} placeHolder={"Enter Username"}></UselessTextInput>
          <UselessTextInput password={true} inputValue={loginInfo.password} onTextChange={updatePassword} styles={styles.loginInput} placeHolder={"Enter Password"}></UselessTextInput>
          <Button title='Login' color={Platform.OS === "ios" ? 'white' : 'transparent'} onPress={() => loginClick(loginInfo.username, loginInfo.password)} styles={styles.btnContainer}></Button>
          {/* TODO: android button looks bad */}
          <StatusBar style="auto" />  

        </View>  
      </ImageBackground>
    </KeyboardAvoidingView>  
    );
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      justifyContent: 'center',
    },
    appName: {
      textAlign: 'center',      
      fontWeight: 'bold',
      fontSize: 40,
      color: 'white',
      marginBottom: '5%',
    },
    loginInput: {
      height: 40,
      width: '80%',
      marginLeft: '10%',
      margin: 12,
      borderBottomWidth: 1,
      borderBottomColor: "white",
      padding: 10,
      marginBottom: '10%',
    },
    btnContainer: {
      marginTop: 14,
    },
    logo: {
      width: 200,
      height: 200,
      borderRadius: 1000,
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: '23%',
      marginTop: '-45%',
      marginBottom: '10%',
      position: 'relative',
    }
  });

  